﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Adresse mail :")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe : ")]
        public string Password { get; set; }
    }
}