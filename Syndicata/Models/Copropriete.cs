﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;


namespace Syndicata.Models
{
    public class Copropriete
    {
        public int Id { get; set; }

        [DisplayName("Nom de copropriété : ")]
        [Required]
        public string Nom { get; set; }

        [DisplayName("Trésorerie de copropriété : ")]
        public decimal Tresorerie { get; set; }

        [Required]
        [Range(1900, 9999)]
        [DisplayName("L'année de construction : ")]
        public int AnneConstruction { get; set; }


        [DisplayName("Syndic de copropriété : ")]
        public Person Syndic { get; set; }
        public int SyndicId { get; set; }

        [DisplayName("Adjoint de copropriété : ")]
        public Person SyndicAdjoint { get; set; }
        public int SyndicAdjointId { get; set; }

        public ICollection<Appartement> Appartements { get; set; }
        public ICollection<AssembleGeneral> AssembleGenerals { get; set; }
        public ICollection<Commande> Commandes { get; set; }
    }
}