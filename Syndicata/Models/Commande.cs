﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;


namespace Syndicata.Models
{
    public class Commande
    {
        public decimal Id { get; set; }

        [DisplayName("Montant : ")]
        public decimal Montant { get; set; }

        [DisplayName("Date de commande : ")]
        public DateTime DateCommande { get; set; }

        [DisplayName("Description : ")]
        public string Description { get; set; }

        [DisplayName("Syndic : ")]
        public Person Syndic { get; set; }
        public int SyndicId { get; set; }

        [DisplayName("Copropriété : ")]
        public Copropriete Copropriete { get; set; }
        public int CoproprieteId { get; set; }
    }
}