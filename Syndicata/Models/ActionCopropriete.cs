﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class ActionCopropriete
    {
        public int Id { get; set; }

        [DisplayName("Libelle :")]
        public string Libelle { get; set; }

        [DisplayName("Motif de l'action :")]
        public string Motif { get; set; }

        [DisplayName("Type de l'action :")]
        public ActionType ActionType { get; set; }

        [DisplayName("Syndic reporteur :")]
        public Person SyndicReporteur { get; set; }
        public int SyndicReporteurId { get; set; }

        [DisplayName("Coproprietaire convoqué :")]
        public Person Copropritaire { get; set; }
        public int CopropritaireId { get; set; }
    }
}