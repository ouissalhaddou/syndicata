﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Syndicata.Models
{
    public class AssembleGeneral
    {
        public int Id { get; set; }

        [DisplayName("Motif : ")]
        public string Motif { get; set; }

        [DisplayName("Résume : ")]
        public string Resume { get; set; }

        [DisplayName("Date d'assemblé général : ")]
        public DateTime DateAssemble { get; set; }

        [DisplayName("Copropriété : ")]
        public Copropriete Copropriete { get; set; }
        public int CoproprieteId { get; set; }
    }
}