﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class Person
    {
        public int Id { get; set; }

        [DisplayName("Civilite : ")]
        public Civilite Civilite { get; set; }

        [DisplayName("Nom : ")]
        public string Nom { get; set; }

        [DisplayName("Prénom : ")]
        public string Prenom { get; set; }

        [DisplayName("Date de naissance : ")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "01-01-1900", "01-01-2900")]
        public DateTime DateNaissance { get; set; }

        [DisplayName("Téléphone : ")]
        public string Telephone { get; set; }

        [DisplayName("Adresse mail : ")]
        public string Email { get; set; }

        [DisplayName("Mot de passe : ")]
        public string Password { get; set; }

        [DisplayName("Rôle : ")]
        public Role RoleId { get; set; }

        [DisplayName("Nom complet : ")]
        public string FullName => $"{Civilite} {Nom} {Prenom}";
    }
}