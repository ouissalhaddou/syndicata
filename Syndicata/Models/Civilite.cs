﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public enum Civilite
    {
        Monsieur,
        Madame
    }
}