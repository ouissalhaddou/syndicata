﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class Paymant
    {
        public int Id { get; set; }

        [DisplayName("Montant : ")]
        public decimal Montant { get; set; }

        [DisplayName("Libelle : ")]
        public string Libelle { get; set; }

        [DisplayName("Date de paymant : ")]
        public DateTime DatePaymant { get; set; }

        [DisplayName("Payant : ")]
        public Person Payant { get; set; }
        public int PayantId { get; set; }

        [DisplayName("Copropriété : ")]
        public Copropriete Copropriete { get; set; }
        public int CoproprieteId { get; set; }
    }
}