﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class Appartement
    {
        public int Id { get; set; }

        [DisplayName("Numéro d'appartement: ")]
        public int NumeroAppartement { get; set; }

        [DisplayName("Surface : ")]
        public int Surface { get; set; }

        [DisplayName("Etage : ")]
        public int Etage { get; set; }

        [DisplayName("Residence : ")]
        public Copropriete Residence { get; set; }
        public int ResidenceId { get; set; }

        [DisplayName("Copropriétaire : ")]
        public Person Propritaire { get; set; }
        public int PropritaireId { get; set; }
    }
}