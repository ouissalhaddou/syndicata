﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Syndicata.Data;
using Syndicata.Models;

namespace Syndicata.Controllers
{
    public class CoproprietesController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: Coproprietes
        public ActionResult Index()
        {
            var coproprietes = db.Coproprietes.Include(c => c.Syndic).Include(c => c.SyndicAdjoint);
            return View(coproprietes.ToList());
        }

        // GET: Coproprietes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copropriete copropriete = db.Coproprietes
                .Include(_ => _.Syndic)
                .Include(_ => _.SyndicAdjoint)
                .Include(_ => _.Appartements)
                .Include(_ => _.AssembleGenerals)
                .Include(_ => _.Commandes)
                .SingleOrDefault(_ => _.Id == id);
            if (copropriete == null)
            {
                return HttpNotFound();
            }
            return View(copropriete);
        }

        // GET: Coproprietes/Create
        public ActionResult Create()
        {
            var syndics = db.People.Where(_ => _.RoleId == Role.Syndic);
            ViewBag.SyndicId = new SelectList(syndics, "Id", "FullName");
            ViewBag.SyndicAdjointId = new SelectList(syndics, "Id", "FullName");
            return View();
        }

        // POST: Coproprietes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom,Tresorerie,AnneConstruction,SyndicId,SyndicAdjointId")] Copropriete copropriete)
        {
            if (ModelState.IsValid)
            {
                db.Coproprietes.Add(copropriete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var syndics = db.People.Where(_ => _.RoleId == Role.Syndic);
            ViewBag.SyndicId = new SelectList(syndics, "Id", "FullName");
            ViewBag.SyndicAdjointId = new SelectList(syndics, "Id", "FullName");
            return View(copropriete);
        }

        // GET: Coproprietes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copropriete copropriete = db.Coproprietes.Find(id);
            if (copropriete == null)
            {
                return HttpNotFound();
            }
            var syndics = db.People.Where(_ => _.RoleId == Role.Syndic);
            ViewBag.SyndicId = new SelectList(syndics, "Id", "FullName");
            ViewBag.SyndicAdjointId = new SelectList(syndics, "Id", "FullName"); 
            return View(copropriete);
        }

        // POST: Coproprietes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom,Tresorerie,AnneConstruction,SyndicId,SyndicAdjointId")] Copropriete copropriete)
        {
            if (ModelState.IsValid)
            {
                db.Entry(copropriete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var syndics = db.People.Where(_ => _.RoleId == Role.Syndic);
            ViewBag.SyndicId = new SelectList(syndics, "Id", "FullName");
            ViewBag.SyndicAdjointId = new SelectList(syndics, "Id", "FullName");
            return View(copropriete);
        }

        // GET: Coproprietes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copropriete copropriete = db.Coproprietes.Find(id);
            if (copropriete == null)
            {
                return HttpNotFound();
            }
            return View(copropriete);
        }

        // POST: Coproprietes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Copropriete copropriete = db.Coproprietes.Find(id);
            db.Coproprietes.Remove(copropriete);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
