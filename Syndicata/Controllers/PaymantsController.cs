﻿using Syndicata.Data;
using Syndicata.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;

namespace Syndicata.Controllers
{
    public class PaymantsController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: Paymants
        public ActionResult Index()
        {
            IQueryable<Paymant> paymants;
            Person user = Session["userInfo"] as Person;
            if(user.RoleId == Role.Copropritaire)
            {
                paymants = db.Paymants
                .Include(p => p.Payant)
                .Include(c => c.Copropriete)
                .Where(_ => _.PayantId == user.Id) ;
            }
            else
            {
                paymants = db.Paymants
                .Include(p => p.Payant)
                .Include(c => c.Copropriete);
            }
             
            return View(paymants.ToList());
        }

        // GET: Paymants/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paymant paymant = db.Paymants
                .Include(_ => _.Payant)
                .Include(c => c.Copropriete)
                .SingleOrDefault(_ => _.Id == id);
            if (paymant == null)
            {
                return HttpNotFound();
            }
            return View(paymant);
        }

        // GET: Paymants/Create
        public ActionResult Create()
        {
            Person user = Session["userInfo"] as Person;

            ViewBag.PayantId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName");
            ViewBag.CoproprieteId = new SelectList(db.Coproprietes.Where(_ => _.SyndicId == user.Id || _.SyndicAdjointId == user.Id), "Id", "Nom");
            return View();
        }

        // POST: Paymants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Montant,Libelle,DatePaymant,PayantId,CoproprieteId")] Paymant paymant)
        {
            if (ModelState.IsValid)
            {
                db.Paymants.Add(paymant);
                db.SaveChanges();
                Copropriete copropriete = db.Coproprietes.SingleOrDefault(_ => _.Id == paymant.CoproprieteId);
                if (copropriete != null)
                {
                    copropriete.Tresorerie += paymant.Montant;
                    db.Entry(copropriete).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            Person user = Session["userInfo"] as Person;
            ViewBag.PayantId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName", paymant.PayantId);
            ViewBag.CoproprieteId = new SelectList(db.Coproprietes.Where(_ => _.SyndicId == user.Id || _.SyndicAdjointId == user.Id), "Id", "Nom");
            return View(paymant);
        }
    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
