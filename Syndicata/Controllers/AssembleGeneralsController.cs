﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Syndicata.Data;
using Syndicata.Models;

namespace Syndicata.Controllers
{
    public class AssembleGeneralsController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: AssembleGenerals
        public ActionResult Index()
        {
            var assembleGenerals = db.AssembleGenerals.Include(a => a.Copropriete);
            return View(assembleGenerals.ToList());
        }

        // GET: AssembleGenerals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssembleGeneral assembleGeneral = db.AssembleGenerals.Find(id);
            if (assembleGeneral == null)
            {
                return HttpNotFound();
            }
            return View(assembleGeneral);
        }

        // GET: AssembleGenerals/Create
        public ActionResult Create()
        {
            ViewBag.CoproprieteId = new SelectList(db.Coproprietes, "Id", "Nom");
            return View();
        }

        // POST: AssembleGenerals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Motif,Resume,Convocation,DateAssemble,CoproprieteId")] AssembleGeneral assembleGeneral)
        {
            if (ModelState.IsValid)
            {
                db.AssembleGenerals.Add(assembleGeneral);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CoproprieteId = new SelectList(db.Coproprietes, "Id", "Nom", assembleGeneral.CoproprieteId);
            return View(assembleGeneral);
        }

        // GET: AssembleGenerals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssembleGeneral assembleGeneral = db.AssembleGenerals.Find(id);
            if (assembleGeneral == null)
            {
                return HttpNotFound();
            }
            ViewBag.CoproprieteId = new SelectList(db.Coproprietes, "Id", "Nom", assembleGeneral.CoproprieteId);
            return View(assembleGeneral);
        }

        // POST: AssembleGenerals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Motif,Resume,Convocation,DateAssemble,CoproprieteId")] AssembleGeneral assembleGeneral)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assembleGeneral).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CoproprieteId = new SelectList(db.Coproprietes, "Id", "Nom", assembleGeneral.CoproprieteId);
            return View(assembleGeneral);
        }

        // GET: AssembleGenerals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssembleGeneral assembleGeneral = db.AssembleGenerals.Find(id);
            if (assembleGeneral == null)
            {
                return HttpNotFound();
            }
            return View(assembleGeneral);
        }

        // POST: AssembleGenerals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssembleGeneral assembleGeneral = db.AssembleGenerals.Find(id);
            db.AssembleGenerals.Remove(assembleGeneral);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
