﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Syndicata.Data;
using Syndicata.Models;

namespace Syndicata.Controllers
{
    public class CommandesController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: Commandes
        public ActionResult Index()
        {
            var commandes = db.Commandes.Include(c => c.Copropriete).Include(c => c.Syndic);
            return View(commandes.ToList());
        }

        // GET: Commandes/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Commande commande = db.Commandes.Find(id);
            if (commande == null)
            {
                return HttpNotFound();
            }
            return View(commande);
        }

        // GET: Commandes/Create
        public ActionResult Create()
        {
            Person user = Session["userInfo"] as Person;
            ViewBag.CoproprieteId = 
                new SelectList(db.Coproprietes.Where(_ => _.SyndicId == user.Id || _.SyndicAdjointId == user.Id), "Id", "Nom");
            return View();
        }

        // POST: Commandes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Montant,Description,CoproprieteId")] Commande commande)
        {
            Person user = Session["userInfo"] as Person;

            if (ModelState.IsValid)
            {
                commande.DateCommande = DateTime.Now;
                commande.SyndicId = user.Id;
                db.Commandes.Add(commande);
                db.SaveChanges();
                var copropriete = db.Coproprietes.SingleOrDefault(_ => _.Id == commande.CoproprieteId);
                if(copropriete != null)
                {
                    copropriete.Tresorerie -= commande.Montant;
                    db.Entry(copropriete).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.CoproprieteId =
                new SelectList(db.Coproprietes.Where(_ => _.SyndicId == user.Id || _.SyndicAdjointId == user.Id), "Id", "Nom");
            ViewBag.SyndicId = user.Id;
            return View(commande);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
