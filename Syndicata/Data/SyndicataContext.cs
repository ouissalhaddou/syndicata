﻿using Syndicata.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Syndicata.Data
{
    public class SyndicataContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SyndicataContext() : base("name=SyndicataContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SyndicataContext, Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }

        public DbSet<Person> People { get; set; }
        public DbSet<Copropriete> Coproprietes { get; set; }
        public DbSet<Appartement> Appartements { get; set; }
        public DbSet<AssembleGeneral> AssembleGenerals { get; set; }
        public DbSet<ActionCopropriete> Actions { get; set; }
        public DbSet<Paymant> Paymants { get; set; }
        public DbSet<Commande> Commandes { get; set; }
    }
}
